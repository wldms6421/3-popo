package com.popo.app.team.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.member.service.MemberVO;
import com.popo.app.team.mapper.TeamMapper;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;

@Service
public class TeamServiceImpl implements TeamService {

	@Autowired
	TeamMapper teamMapper;

	@Override
	public List<TeamVO> getTeamMember() {
		// TODO Auto-generated method stub
		return teamMapper.getTeamMember();
	}

	@Override
	public String findTeamName() {
		// TODO Auto-generated method stub
		return teamMapper.findTeamName();
	} 
	
}
