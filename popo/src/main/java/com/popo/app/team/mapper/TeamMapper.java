package com.popo.app.team.mapper;

import java.util.List;

import com.popo.app.member.service.MemberVO;
import com.popo.app.team.service.TeamVO;

public interface TeamMapper {

	
	public List<TeamVO> getTeamMember();
	
	public String findTeamName();
}
