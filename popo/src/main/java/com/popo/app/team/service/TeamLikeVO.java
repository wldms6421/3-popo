package com.popo.app.team.service;

import lombok.Data;

@Data
public class TeamLikeVO {

	//mem_no 회원번호 필요
	String likeType;  //좋아요타입 팀/개인
	int artTeamNo;	  //팀/아티스트번호
	int teamLike;	  //좋아요여부
	
}
