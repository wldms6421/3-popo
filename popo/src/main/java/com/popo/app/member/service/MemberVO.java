package com.popo.app.member.service;

import lombok.Data;

@Data
public class MemberVO {
	int memNo;
	String memId;
	String memPw;
	String memName;
	String memNickname;
	String memRegistNo;
	String memTel;
	String memEmail;
	String memAdress;
	String memAccountNum;
	String memRating;
	int teamNo;
	int memReportCount;
	String memProfilePic;
	String memPlaceCertiInfo;
	String positionName;
	int chatListNo;
	int memStatus;
}
