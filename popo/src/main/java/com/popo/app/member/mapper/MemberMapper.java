package com.popo.app.member.mapper;

import java.util.List;

import com.popo.app.member.service.MemberVO;


public interface MemberMapper {
	
	public List<MemberVO> selectAllEmpList();
	
}
