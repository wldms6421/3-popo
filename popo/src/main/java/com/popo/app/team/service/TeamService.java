package com.popo.app.team.service;

import java.util.List;

import com.popo.app.member.service.MemberVO;

public interface TeamService {

	
	public List<TeamVO> getTeamMember();
	
	public String findTeamName();
}
