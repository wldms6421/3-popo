package com.popo.app.team.service;


import java.util.Date;

import lombok.Data;

@Data
public class TeamVO {
	
	int teamNo;
	String teamName;
	String teamTarget;  //팀목표
	String teamSpe;		//팀특징
	Date teamStart;
	int teamNum;		//팀인원수
	String teamImage;	//팀대표이미지
	int teamStatus;		//공개상태 0,1
	
	String memName;
	String memProfilePic;
	String genreName;
	String positionName;
	
	
	
}
