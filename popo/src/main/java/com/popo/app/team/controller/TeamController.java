package com.popo.app.team.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.popo.app.member.service.MemberVO;
import com.popo.app.team.service.TeamService;
import com.popo.app.team.service.TeamVO;

@Controller
public class TeamController {

	
	@Autowired
	TeamService teamService;
	
	//팀 히스토리 첫페이지 - 이력
	@GetMapping("/team/teampage")
	public String teamhistory() {
		
		return "/team/teamhistory";
	}
	
	//팀 히스토리-공연일정
	@GetMapping("/team/teamplan")
	public String teamplan() {
		
		return "/team/teamplan";
	}
	
	//팀 히스토리 - 팀정보
	@GetMapping("/team/teamhispage")
	public String teamhispage(Model model) {
		
		List<TeamVO> teamMemList =  teamService.getTeamMember();
		model.addAttribute("teamMemberList", teamService.getTeamMember());
		model.addAttribute("teamName", teamService.findTeamName());
		//model.addAttribute("teamMemberList",teamMemList); @ModelAttribute TeamVO teamVO,
		return "/team/teamhistory";
	}
		
		
	
}
