package com.popo.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(basePackages="com.popo.app.**.mapper")
public class PopoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PopoApplication.class, args);
	}

}
