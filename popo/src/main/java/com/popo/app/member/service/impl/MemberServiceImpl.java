package com.popo.app.member.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.popo.app.member.mapper.MemberMapper;
import com.popo.app.member.service.MemberService;
import com.popo.app.member.service.MemberVO;

@Service
public class MemberServiceImpl implements MemberService {

	@Autowired
	MemberMapper memberMapper; 
	
	@Override
	public List<MemberVO> getAllList() {
		// TODO Auto-generated method stub
		return memberMapper.selectAllEmpList();
	}

}
